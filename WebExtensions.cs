using Drive.Config;
using Drive.Extensions.Proxy;
using System.Net.WebSockets;

namespace Drive.Extensions;

public static class WebExtensions
{
    public static IServiceCollection AddProxyMiddleware(
      this IServiceCollection services,
      IConfiguration configuration)
    {
        services.AddProxy((Action<SharedProxyOptions>)(options =>
        {
            options.PrepareUri = new Func<HttpRequest, Uri>(ProxyHelpers.PrepageUri);
            options.PrepareRequest = new Func<HttpRequest, HttpRequestMessage, Task>(ProxyHelpers.PrepageForRequest);
            // options.PrepareForWebSocket = new Func<HttpRequest, ClientWebSocketOptions, Task>(ProxyHelpers.PrepageForWebSocketRequest);
        }));
        // services.Configure<ApiConfig>(configuration);
        return services;
    }
    public static IServiceCollection AddProxy(
          this IServiceCollection services,
          Action<SharedProxyOptions> configureOptions)
    {
        if (services == null)
            throw new ArgumentNullException(nameof(services));
        if (configureOptions == null)
            throw new ArgumentNullException(nameof(configureOptions));
        services.Configure<SharedProxyOptions>(configureOptions);
        return services.AddSingleton<ProxyService>();
    }
    
    public static IApplicationBuilder UseProxyMiddleware(this IApplicationBuilder builder)
    {
        builder.RunProxy((PathString)"/call");
        return builder.UseMiddleware<ProxyUtilsMiddleware>();
    }
}