using Microsoft.AspNetCore.SignalR;

namespace iofty.SignalR;

public abstract class BaseHub : Hub
{
    public override async Task OnConnectedAsync()
    {
        // var orgId = Context.User?.Claims?.FirstOrDefault(m => m.Type == "orgid").Value;

        // if (orgId != null)
        //     await base.Groups.AddToGroupAsync(this.Context.ConnectionId, $"org:{orgId}");
        Console.WriteLine("OnConnectedAsync");
        await base.OnConnectedAsync();
    }

    public override async Task OnDisconnectedAsync(Exception exception)
    {
        // var orgId = Context.User?.Claims?.FirstOrDefault(m => m.Type == "orgid").Value;

        // if (orgId != null)
        //     await base.Groups.RemoveFromGroupAsync(this.Context.ConnectionId, $"org:{orgId}");
        Console.WriteLine("OnDisconnectedAsync");

        await base.OnDisconnectedAsync(exception);
    }
}

public class UserProvider : IUserIdProvider
{
    public string GetUserId(HubConnectionContext connection)
    {
        var uid = connection.User?.Claims?.FirstOrDefault(m => m.Type == "sub")?.Value;
       
        return uid;
    }
}