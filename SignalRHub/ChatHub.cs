using Microsoft.AspNetCore.SignalR;

namespace iofty.SignalR;

public class ChatHub : BaseHub
{
    public async Task Ping(string msg)
    {
        await Clients.Caller.SendAsync("Pong", msg);
    }
    public async Task SendMessage(string user, string message)
           => await Clients.All.SendAsync("ReceiveMessage", user, message);
}

