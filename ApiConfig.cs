namespace Drive.Config;

public class ApiConfig
{
    public List<Api> Apis { get; set; }

    public int? ProxyRequestTimeout { get; set; }
}

public class Api
{
    public string Id { get; set; }
    public string Url { get; set; }
}