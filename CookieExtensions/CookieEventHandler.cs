using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Drive.Extensions.Cookie;

public class CookieEventHandler : CookieAuthenticationEvents
{
//     private readonly
// #nullable disable
    // ILogoutSessionManager LogoutSessions;

    // public CookieEventHandler(ILogoutSessionManager LogoutSessions) => this.LogoutSessions = LogoutSessions;

    public override async Task ValidatePrincipal(CookieValidatePrincipalContext context)
    {
        await base.ValidatePrincipal(context);
        if (!context.Principal.Identity.IsAuthenticated)
            return;
        // if (!await this.LogoutSessions.IsLoggedOut(context.Principal.FindFirst("sub")?.Value, context.Principal.FindFirst("sid")?.Value))
        //     return;
        context.RejectPrincipal();
        await context.HttpContext.SignOutAsync();
    }

    public override async Task SignedIn(CookieSignedInContext context) => await base.SignedIn(context);

    public override async Task SigningIn(CookieSigningInContext context) => await base.SigningIn(context);
}