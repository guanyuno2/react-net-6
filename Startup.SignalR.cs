namespace iofty.SignalR;

public static class StartupExtensions
{
    public static void AddSignalRIofty(this IServiceCollection services, IConfiguration config)
    {
        services.AddSignalR();
        services.AddSingleton<Microsoft.AspNetCore.SignalR.IUserIdProvider, UserProvider>();
    }
}