using System.Security.Claims;
using iofty.SignalR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace Drive.Controllers;


[Route("[controller]")]
public class TestController : ControllerBase
{
    private readonly IHubContext<ChatHub> _hub;
    public TestController(IHubContext<ChatHub> hub)
    {
        _hub = hub;
    }

    // [Authorize]
    [Route("/test/ping")]
    public async Task<object> Ping()
    {
        await _hub.Clients.All.SendAsync("Ping", "TY van abc");
        return null;
    }
}