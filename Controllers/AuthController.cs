using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Drive.Controllers;
[Route("[controller]")]
public class AuthController : ControllerBase
{
    private readonly IConfiguration _configuration;
    public AuthController(IConfiguration configuration)
    {
        _configuration = configuration;
    }
    [Route("/auth/user")]
    public AuthUser GetUser()
    {
        if (User?.Identity != null && User.Identity.IsAuthenticated)
        {
            return new AuthUser()
            {
                UserId = User.Claims.FirstOrDefault(m => m.Type == "sub").Value,
                Name = User.Claims.FirstOrDefault(m => m.Type == "name").Value,
                Authority = _configuration["App:Authority"]

            };
        }
        else
            return new AuthUser()
            {
                UserId = ""
            };
    }

    public class AuthUser
    {
        public string Name { get; set; }
        public string UserId { get; set; }
        public string Authority { get; set; }
        public List<string> Roles { get; set; } = new List<string>();
    }

    [Authorize]
    [Route("/auth/logout")]
    public ActionResult Logout(string returnUri)
    {
        return new SignOutResult(new[] { "Cookies", "oidc" });
    }

    [Authorize]
    [Route("/auth/login")]
    public ActionResult Login(string returnUri)
    {
        if (returnUri == null)
            returnUri = "/";

        if (!Uri.IsWellFormedUriString(returnUri, UriKind.Relative))
        {
            if (Uri.IsWellFormedUriString(returnUri, UriKind.Absolute))
            {
                var returnHost = new Uri(returnUri).Host;
                if (!Request.Host.Equals(returnHost))
                    returnUri = "/";
            }
        }

        return Redirect(returnUri);
    }
}

