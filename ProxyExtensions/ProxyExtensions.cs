using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using Microsoft.Extensions.Primitives;

namespace Drive.Extensions.Proxy;

public static class ProxyExtensions
{
    public static void RunProxy(this
        IApplicationBuilder app, PathString sourcePathBase)
    {
        if (app == null)
            throw new ArgumentNullException(nameof(app));
        ProxyOptions options = new ProxyOptions()
        {
            SourcePathBase = sourcePathBase
        };
        app.UseMiddleware<ProxyMiddleware>((object)Microsoft.Extensions.Options.Options.Create<ProxyOptions>(options));
    }

    public static HttpRequestMessage CreateProxyHttpRequest(this HttpContext context, Uri uri)
    {
        HttpRequest request = context.Request;
        request.ContentType = "application/json";
        HttpRequestMessage proxyHttpRequest = new HttpRequestMessage();
        string method = request.Method;
        if (!HttpMethods.IsGet(method) && !HttpMethods.IsHead(method) && !HttpMethods.IsDelete(method) && !HttpMethods.IsTrace(method))
        {
            StreamContent streamContent = new StreamContent(request.Body);
            proxyHttpRequest.Content = (HttpContent)streamContent;
        }
        foreach (KeyValuePair<string, StringValues> header in (IEnumerable<KeyValuePair<string, StringValues>>)request.Headers)
        {
            HttpRequestHeaders headers1 = proxyHttpRequest.Headers;
            string key1 = header.Key;
            StringValues stringValues = header.Value;
            string[] array1 = stringValues.ToArray();
            if (!headers1.TryAddWithoutValidation(key1, (IEnumerable<string>)array1) && proxyHttpRequest.Content != null)
            {
                HttpContent content = proxyHttpRequest.Content;
                if (content != null)
                {
                    HttpContentHeaders headers2 = content.Headers;
                    string key2 = header.Key;
                    stringValues = header.Value;
                    string[] array2 = stringValues.ToArray();
                    headers2.TryAddWithoutValidation(key2, (IEnumerable<string>)array2);
                }
            }
        }
        proxyHttpRequest.Headers.Host = uri.Authority;
        proxyHttpRequest.RequestUri = uri;
        proxyHttpRequest.Method = new HttpMethod(request.Method);
        return proxyHttpRequest;
    }

    public static Task<HttpResponseMessage> SendProxyHttpRequest(
      this HttpContext context,
      HttpRequestMessage requestMessage)
    {
        if (requestMessage == null)
            throw new ArgumentNullException(nameof(requestMessage));
        return context.RequestServices.GetRequiredService<ProxyService>().Client.SendAsync(requestMessage, HttpCompletionOption.ResponseHeadersRead, context.RequestAborted);
    }

    public static async Task CopyProxyHttpResponse(
      this HttpContext context,
      HttpResponseMessage responseMessage)
    {
        if (responseMessage == null)
            throw new ArgumentNullException(nameof(responseMessage));
        HttpResponse response = context.Response;
        response.StatusCode = (int)responseMessage.StatusCode;
        foreach (KeyValuePair<string, IEnumerable<string>> header in (HttpHeaders)responseMessage.Headers)
            response.Headers[header.Key] = (StringValues)header.Value.ToArray<string>();
        foreach (KeyValuePair<string, IEnumerable<string>> header in (HttpHeaders)responseMessage.Content.Headers)
            response.Headers[header.Key] = (StringValues)header.Value.ToArray<string>();
        ((IDictionary<string, StringValues>)response.Headers).Remove("transfer-encoding");
        using (Stream responseStream = await responseMessage.Content.ReadAsStreamAsync())
            await responseStream.CopyToAsync(response.Body, 81920, context.RequestAborted);
        response = (HttpResponse)null;
    }

    public static async Task ProxyRequest(this HttpContext context)
    {
        ProxyService proxyService = context != null
            ? context.RequestServices.GetRequiredService<ProxyService>()
            : throw new ArgumentNullException(nameof(context));
        Uri uri = proxyService.Options.PrepareUri(context.Request);
        if (uri == (Uri)null)
            throw new ArgumentNullException("destinationUri");
        using HttpRequestMessage requestMessage = context.CreateProxyHttpRequest(uri);

        Func<HttpRequest, HttpRequestMessage, Task> prepareRequest = proxyService.Options.PrepareRequest;
        if (prepareRequest != null)
            await prepareRequest(context.Request, requestMessage);
        try
        {
            using HttpResponseMessage responseMessage = await context.SendProxyHttpRequest(requestMessage);
            await context.CopyProxyHttpResponse(responseMessage);
        }
        catch
        {
            // var interpolatedStringHandler = new DefaultInterpolatedStringHandler(40, 1);
            // interpolatedStringHandler.AppendLiteral("Error occurred when trying to proxy to: ");
            // interpolatedStringHandler.AppendFormatted<Uri>(requestMessage.RequestUri);
            Console.WriteLine("Error occurred when trying to proxy");
            throw;
        }

    }
}
