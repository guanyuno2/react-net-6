using Microsoft.Extensions.Options;

namespace Drive.Extensions.Proxy;

public class ProxyMiddleware
{
    private const int DefaultWebSocketBufferSize = 4096;
    private readonly RequestDelegate _next;
    private readonly ProxyOptions _options;
    private static readonly string[] NotForwardedWebSocketHeaders = new string[5]
    {
      "Connection",
      "Host",
      "Upgrade",
      "Sec-WebSocket-Key",
      "Sec-WebSocket-Version"
    };

    public ProxyMiddleware(RequestDelegate next, IOptions<ProxyOptions> options)
    {
        if (next == null)
            throw new ArgumentNullException(nameof(next));
        if (options == null)
            throw new ArgumentNullException(nameof(options));
        this._next = next;
        this._options = options.Value;
    }

    public Task Invoke(HttpContext context)
    {
        if (context == null)
            throw new ArgumentNullException(nameof(context));
        return !context.Request.Path.StartsWithSegments(this._options.SourcePathBase) ? this._next(context) : context.ProxyRequest();
    }
}

public class ProxyOptions
{
    public PathString SourcePathBase { get; set; }
    public string Host { get; set; }
}