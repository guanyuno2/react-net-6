using System.Net.Http.Headers;
using System.Net.WebSockets;
using Drive.Config;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;

namespace Drive.Extensions.Proxy;

public static class ProxyHelpers
{
    private static HttpClient client = new HttpClient((HttpMessageHandler)new HttpClientHandler()
    {
        AllowAutoRedirect = false,
        UseCookies = false
    });

    // public static string ApiSegment => apiSegment;

    public static async Task PrepageForRequest(
      HttpRequest originalRequest,
      HttpRequestMessage message)
    {
        HttpContext context = originalRequest.HttpContext;
        // message.Headers.Add("X-Forwarded-Host", originalRequest.Host.Host);
        // string clientIp = context.TryGetClientIP();
        // if (!message.Headers.TryAddWithoutValidation("X-OSS-Client-IP", clientIp) && message.Content != null)
        //     message.Content.Headers.TryAddWithoutValidation("X-OSS-Client-IP", clientIp);
        string parameter = await context.GetTokenAsync("access_token") ?? context.Items[(object)"access_token"] as string;
        if (parameter == null)
        {
            context = (HttpContext)null;
        }
        else
        {
            message.Headers.Authorization = new AuthenticationHeaderValue("Bearer", parameter);
            context = (HttpContext)null;
        }
    }

    public static async Task PrepageForWebSocketRequest(
        HttpRequest originalRequest,
        ClientWebSocketOptions option)
    {
        string tokenAsync = await originalRequest.HttpContext.GetTokenAsync("access_token");
        if (tokenAsync == null)
            return;
        option.SetRequestHeader("Authorization", "Bearer " + tokenAsync);
    }

    public static bool IsAjaxRequest(this HttpRequest request)
    {
        if (request == null)
            throw new ArgumentNullException(nameof(request));
        return request.Headers != null && request.Headers["X-Requested-With"] == "XMLHttpRequest";
    }

    public static void RedirectToRoot(this HttpResponse httpResponse)
    {
        if (httpResponse.HttpContext.Request.PathBase.HasValue)
            httpResponse.Redirect((string)httpResponse.HttpContext.Request.PathBase);
        else
            httpResponse.Redirect("/");
    }

    public static string GetRoot(this HttpRequest httpResponse) => httpResponse.PathBase.HasValue ? (string)httpResponse.PathBase : "/";

    public static Uri PrepageUri(HttpRequest originalRequest)
    {
        HttpContext httpContext = originalRequest.HttpContext;
        ApiConfig apiConfig = originalRequest.HttpContext.RequestServices.GetRequiredService<IOptions<ApiConfig>>().Value;
        string str1 = originalRequest.Path.Value;
        int startIndex = str1.IndexOf('/', "/call".Length + 1);
        string apiName = str1.Substring("/call".Length + 1, startIndex - 1 - "/call".Length);
        string str2 = str1.Substring(startIndex);
        return new UriBuilder(new Uri("https://api.iofty.online" + str2))
        {
            Query = originalRequest.QueryString.ToString()
        }.Uri;
    }

    public static Uri PrepageUri(ApiConfig config, Uri input)
    {
        Uri uri = new Uri("http://example.com" + input.OriginalString);
        string absolutePath = uri.AbsolutePath;
        int startIndex = absolutePath.IndexOf('/', "/call".Length + 1);
        string apiName = absolutePath.Substring("/call".Length + 1, startIndex - 1 - "/call".Length);
        string str = absolutePath.Substring(startIndex);
        return new UriBuilder(new Uri((config.Apis.FirstOrDefault<Api>((Func<Api, bool>)(m => m.Id == apiName)) ?? config.Apis.FirstOrDefault<Api>()).Url + str))
        {
            Query = uri.Query
        }.Uri;
    }

    public static async Task<HttpResponseMessage> SendApiCallRequest(
      this HttpContext httpContext,
      HttpRequestMessage request)
    {
        request.RequestUri = ProxyHelpers.PrepageUri(httpContext.RequestServices.GetRequiredService<IOptions<ApiConfig>>().Value, request.RequestUri);
        await ProxyHelpers.PrepageForRequest(httpContext.Request, request);
        return await ProxyHelpers.client.SendAsync(request);
    }

    private static string TryGetClientIP(this HttpContext context)
    {
        string headerValueAs = context.GetHeaderValueAs<string>("X-Real-IP");
        if (headerValueAs != null)
            return headerValueAs;
        return context?.Connection?.RemoteIpAddress?.ToString();
    }

    private static T GetHeaderValueAs<T>(this HttpContext context, string headerName)
    {
        StringValues stringValues;
        return (context?.Request?.Headers?.TryGetValue(headerName, out stringValues) ?? false)
         && !string.IsNullOrEmpty(stringValues.ToString()) ? (T)Convert.ChangeType((object)stringValues.ToString(), typeof(T)) : default(T);
    }
}