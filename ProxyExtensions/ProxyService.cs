using Drive.Config;
using Microsoft.Extensions.Options;

namespace Drive.Extensions.Proxy;
public class ProxyService
{
    public ProxyService(IOptions<SharedProxyOptions> options, IOptions<ApiConfig> apiOptions)
    {
        this.Options = options != null ? options.Value : throw new ArgumentNullException(nameof(options));
        HttpMessageHandler handler = this.Options.MessageHandler;
        if (handler == null)
            handler = (HttpMessageHandler)new HttpClientHandler()
            {
                AllowAutoRedirect = false,
                UseCookies = false
            };
        this.Client = new HttpClient(handler);
        int? proxyRequestTimeout = (int?)apiOptions?.Value?.ProxyRequestTimeout;
        if (!proxyRequestTimeout.HasValue)
            return;
        this.Client.Timeout = TimeSpan.FromSeconds((double)proxyRequestTimeout.Value);
    }

    public SharedProxyOptions Options { get; private set; }

    internal HttpClient Client { get; private set; }
}