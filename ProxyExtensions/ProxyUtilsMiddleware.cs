using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Primitives;

namespace Drive.Extensions.Proxy;
public class ProxyUtilsMiddleware
{
    private readonly
#nullable disable
    RequestDelegate _next;
    private const string BACK_CHANNEL_LOGOUT = "/back_channel_logout";
    private const string REFRESH_SESSION = "/refresh_session";
    private const string REGISTER_ACCOUNT = "/account/register";
    private const string LOGIN_ACCOUNT = "/account/login";
    private const string LOGIN_ACCOUNT_CALLBACK = "/account/login_callback";
    private readonly IConfiguration _config;
    private static string[] SupportParameters = new string[9]
    {
      "prompt",
      "locale",
      "utm_source",
      "utm_medium",
      "utm_campaign",
      "utm_content",
      "utm_term",
      "ref",
      "src"
    };

    public ProxyUtilsMiddleware(RequestDelegate next, IConfiguration config)
    {
        this._next = next;
        this._config = config;
    }

    private string getReturnUrl(HttpContext httpContext)
    {
        List<string> source = this._config.GetSection("TrustedRedirects").Get<List<string>>();
        string root = (string)httpContext.Request.Query["returnUrl"];
        if (string.IsNullOrEmpty(root))
        {
            root = httpContext.Request.GetRoot();
        }
        else
        {
            Uri returnURI;
            if (!Uri.TryCreate(root, UriKind.RelativeOrAbsolute, out returnURI) || returnURI.IsAbsoluteUri && source != null && !source.Any<string>((Func<string, bool>)(trusted => returnURI.Host.EndsWith(trusted))))
                root = httpContext.Request.GetRoot();
        }
        return root;
    }

    private string getRedirectUrl(HttpContext httpContext)
    {
        string returnUrl = this.getReturnUrl(httpContext);
        return QueryHelpers.AddQueryString(new UriBuilder(httpContext.Request.GetDisplayUrl())
        {
            Path = (httpContext.Request.PathBase + "/account/login_callback"),
            Query = ((string)null)
        }.Uri.AbsoluteUri, "returnUrl", returnUrl);
    }

    public async Task Invoke(HttpContext httpContext)
    {
        var test = httpContext.User;
        PathString path = httpContext.Request.Path;
        if (path.StartsWithSegments((PathString)"/account/login_callback"))
        {
            AuthenticateResult result = await httpContext.AuthenticateAsync("oidc");
            if (result != null && result.Succeeded)
            {
                await httpContext.SignOutAsync("oidc");
                await httpContext.SignInAsync(result.Principal, result.Properties);
                httpContext.Response.Redirect(this.getReturnUrl(httpContext));
            }
            else
                httpContext.Response.Redirect(this.getReturnUrl(httpContext));
        }
        else
        {
            path = httpContext.Request.Path;
            if (path.StartsWithSegments((PathString)"/account/login"))
            {
                AuthenticationProperties properties = new AuthenticationProperties();
                if (httpContext.Request.Query.ContainsKey("orgid"))
                    properties.SetParameter<StringValues>("orgid", httpContext.Request.Query["orgid"]);
                ProxyUtilsMiddleware.AddParameters(httpContext, properties);
                properties.RedirectUri = this.getRedirectUrl(httpContext);
                await httpContext.ChallengeAsync("oidc", properties);
            }
            else
            {
                path = httpContext.Request.Path;
                if (path.StartsWithSegments((PathString)"/account/register"))
                {
                    AuthenticationProperties properties = new AuthenticationProperties();
                    properties.SetParameter<string>("register", "true");
                    properties.RedirectUri = this.getRedirectUrl(httpContext);
                    ProxyUtilsMiddleware.AddParameters(httpContext, properties);
                    await httpContext.ChallengeAsync("oidc", properties);
                }
                else
                {
                    path = httpContext.Request.Path;
                    if (path.StartsWithSegments((PathString)"/back_channel_logout"))
                    {
                        // string logoutToken = (string)httpContext.Request.Form["logout_token"];
                        // if (logoutToken != null)
                        //     await httpContext.RequestServices.GetRequiredService<ILogoutSessionManager>().Add(logoutToken);
                        // httpContext.Response.StatusCode = 200;
                    }
                    else
                    {
                        path = httpContext.Request.Path;
                        if (path.StartsWithSegments((PathString)"/refresh_session"))
                        {
                            bool? isAuthenticated = httpContext.User?.Identity?.IsAuthenticated;
                            bool flag = true;
                            if (isAuthenticated.GetValueOrDefault() == flag & isAuthenticated.HasValue)
                            {
                                await httpContext.SignOutAsync("Cookies");
                                string header = (string)httpContext.Request.Headers["Referer"];
                                if (header != null && Uri.IsWellFormedUriString(header, UriKind.Absolute))
                                {
                                    if (new Uri(header).Host == httpContext.Request.Host.Host)
                                    {
                                        httpContext.Response.Redirect(header);
                                        return;
                                    }
                                }
                                else
                                {
                                    httpContext.Response.RedirectToRoot();
                                    return;
                                }
                            }
                        }
                        await this._next(httpContext);
                    }
                }
            }
        }
    }

    private static void AddParameters(HttpContext httpContext, AuthenticationProperties properties)
    {
        foreach (string supportParameter in ProxyUtilsMiddleware.SupportParameters)
        {
            if (httpContext.Request.Query.ContainsKey(supportParameter))
                properties.SetParameter<StringValues>(supportParameter, httpContext.Request.Query[supportParameter]);
        }
    }
}