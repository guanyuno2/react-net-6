using System.Net.WebSockets;

namespace Drive.Extensions.Proxy;

public class SharedProxyOptions
{
    private int? _webSocketBufferSize;

    public HttpMessageHandler MessageHandler { get; set; }

    public Func<HttpRequest, HttpRequestMessage, Task> PrepareRequest { get; set; }

    public Func<HttpRequest, Uri> PrepareUri { get; set; }

    public Func<HttpRequest, ClientWebSocketOptions, Task> PrepareForWebSocket { get; set; }

    public TimeSpan? WebSocketKeepAliveInterval { get; set; }

    public int? WebSocketBufferSize
    {
        get => this._webSocketBufferSize;
        set
        {
            if (value.HasValue && value.Value <= 0)
                throw new ArgumentOutOfRangeException(nameof(value));
            this._webSocketBufferSize = value;
        }
    }
}