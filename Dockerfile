# Publish Spa
FROM node:lts-alpine as node-build
WORKDIR /app
COPY ./ClientApp ./
# cd to csproj <SpaRoot>
# restore npm dependencies
RUN npm install --legacy-peer-deps
# run csproj <SpaBuildCommand>
RUN npm run build -- --prod

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS dotnet-build
WORKDIR /app
COPY . ./
# Copy SpaRoot
COPY --from=node-build /app/build ./ClientApp/build
RUN dotnet restore .
# Skip Spa build during publish
RUN dotnet publish --no-restore -c Release -o ./out . /p:SkipSpaBuild=True

# Build application image
FROM mcr.microsoft.com/dotnet/aspnet:6.0
WORKDIR /app
COPY --from=dotnet-build /app/out /app
ENV ASPNETCORE_ENVIRONMENT Staging
ENTRYPOINT ["dotnet", "Iofty.Client.dll"]

