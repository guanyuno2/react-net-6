const { createProxyMiddleware } = require('http-proxy-middleware');
const { env } = require('process');

const target = env.ASPNETCORE_HTTPS_PORT ? `https://localhost:${env.ASPNETCORE_HTTPS_PORT}` :
  env.ASPNETCORE_URLS ? env.ASPNETCORE_URLS.split(';')[0] : 'http://localhost:43417';

const context =  [
  "/WeatherForecast",
  "/call",
  "/auth",
  "/signin-oidc",
  "/Account",
  "/account",
  "/back_channel_logout",
  "/refresh_session",
  "/iofty_hub",
  "/test"
];


// "/auth",
// "/Account",
// "/signin-oidc",
// "/back_channel_logout",
// "/refresh_session",
// "/account",
// "/call"

module.exports = function(app) {
  const appProxy = createProxyMiddleware(context, {
    target: target,
    secure: false,
    ws: true
    
  });

  app.use(appProxy);
};
