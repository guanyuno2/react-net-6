import { EFileType } from "./components/IconFileType";
import { IFolder } from "./components/ListFolder/_interfaces";
import { S3Client, PutObjectCommand, PutObjectRequest } from "@aws-sdk/client-s3";

const ACCOUNT_ID = '353b244ce35f10ba163ed01a235a1aaa'
const ACCESS_KEY_ID = '62eeb0baf527ab79c46348caf0faac65'
const SECRET_ACCESS_KEY = '31237fe776a2396ba68c5c28a9595564ca895cb31ae36ac8ec4693fc4ebb0191'


let env = {
    baseUrl: document.location.origin,
    layout: null
}

export const s3client = new S3Client({
    region: "auto",
    endpoint: `https://${ACCOUNT_ID}.r2.cloudflarestorage.com`,
    credentials: {
        accessKeyId: ACCESS_KEY_ID,
        secretAccessKey: SECRET_ACCESS_KEY,
    },

});

export const StaticObjStorage = 'https://static.iofty.online'

export const Env = env;
export const parseDate = (dateISO: string) => {
    const date = new Date(dateISO);
    const day = date.getDate().toString().padStart(2, '0'); // Get day and ensure 2-digit format
    const month = (date.getMonth() + 1).toString().padStart(2, '0'); // Get month (adding 1 because January is 0) and ensure 2-digit format
    const year = date.getFullYear().toString().slice(-2); // Get year and extract last 2 digits


    const formattedDate = `${day}/${month}/${year}`;
    return formattedDate
}

export const getFileType = (fileName: string) => {
    let type = '' as EFileType
    if (fileName.toLowerCase().includes("csv"))
        type = '.csv'
    if (fileName.toLowerCase().includes("pdf"))
        type = '.pdf'
    if (fileName.toLowerCase().includes("docx"))
        type = '.docx'
    if (fileName.toLowerCase().includes("doc"))
        type = '.doc'
    if (fileName.toLowerCase().includes("txt"))
        type = '.txt'
    if (fileName.toLowerCase().includes("ppt"))
        type = '.ppt'
    if (fileName.toLowerCase().includes("xsl"))
        type = '.xsl'
    if (fileName.toLowerCase().includes("xls"))
        type = '.xsl'
    if (fileName.toLowerCase().includes("xslx"))
        type = '.xsl'
    if (fileName.toLowerCase().includes("xlsx"))
        type = '.xsl'
    if (fileName.toLowerCase().includes("zip"))
        type = '.zip'
    if (fileName.toLowerCase().includes("rar"))
        type = '.rar'
    if (fileName.toLowerCase().includes("jpg"))
        type = '.jpg'
    if (fileName.toLowerCase().includes("png"))
        type = '.png'
    if (fileName.toLowerCase().includes("mp4"))
        type = '.mp4'
    if (fileName.toLowerCase().includes("mp3"))
        type = '.mp3'

    return type
}
export const createFolder = async (folder: IFolder, callback: (isSuccess: boolean) => void) => {
    let rsp = await api_call_post('folder', folder)
    if (rsp.data) {
        // setIsOpenNewFolder(false)
        // toast({
        //     title: `Folder ${folder.name} created sucessfully.`,
        //     // description: "We've created your account for you.",
        //     status: 'success',
        //     duration: 3000,
        //     isClosable: true
        // })
        callback(true)
    }
    else {
        // toast({
        //     title: `${rsp.message}`,
        //     // description: "We've created your account for you.",
        //     status: 'error',
        //     duration: 3000,
        //     isClosable: true
        // })
        callback(false)
    }
}

export const api_call = async (method: string, body: any, path: string): Promise<any> => {
    let url = `/call/drive_api/${path}`;
    try {
        let rp = await fetch(url, {
            method: method,
            body: body ? JSON.stringify(body) : null,
            credentials: 'include',
            mode: 'no-cors',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
        if (rp.status == 401) {
            // if (env.layout) {
            //     env.layout.openPopupAuthorizeException();
            //     await checkRefreshSessionFinish();
            return await api_call(method, body, path);
            // }
        } else if (rp.status == 403) {
            // if (env.layout)
            //     env.layout.redirectPagePermission(null)
        } else if (rp.status == 200 || rp.status == 201 || rp.status == 304) {
            return await rp.json();
        }

    }
    catch (e) {
        // if (env.layout) {
        //     env.layout.ShowMessage('error', 'Xin lỗi, đã có lỗi khi thực hiện');
        // }

        throw e;
    }
    finally {

    }
}

export const call_get = (path: string): Promise<any> => {
    return api_call('GET', null, path);
}
export const call_post = (path: string, body: any): Promise<any> => {
    return api_call('POST', body, path);
}

export const call_put = (path: string, body: any): Promise<any> => {
    return api_call('PUT', body, path);
}

export const call_delete = (path: string): Promise<any> => {
    return api_call('DELETE', null, path);
}

export const api_call_get = (path: string): Promise<any> => { return call_get(path) }
export const api_call_post = (path: string, body: any): Promise<any> => { return call_post(path, body) }
export const api_call_put = (path: string, body: any): Promise<any> => { return call_put(path, body) }
export const api_call_delete = (path: string): Promise<any> => { return call_delete(path) }
