
// @flow
import * as React from "react";
import { useEffect } from "react";
import useSWR from "swr";
import { Header } from "./components/Layout/Header";
import { LeftMenu } from "./components/Layout/LeftMenu";

type Props = {
	children?: React.ReactNode;
};
export const App = (props: Props) => {
	const { data } = useSWR("/auth/user");
	useEffect(() => {
		//this code for fixing process not working on browser (async-lock)
		window.process = {
			...window.process,
		};

		if (data?.userId == "") window.location.href = "/auth/login";
		else {
			document['user'] = data
		}
	}, [data?.userId]);

	if (data?.userId?.length > 0) return <>
		<LeftMenu />
		<div className="body-container">
			<Header name={data.name} />
			<div className="body-app">
				{props.children}
			</div>
		</div>

	</>;

	return <div></div>;
};


