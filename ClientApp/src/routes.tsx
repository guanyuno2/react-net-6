import React from "react";
import { Routes, Route, Navigate } from "react-router-dom"
import { AuthorizeRoute } from "./components/Authorization/AuthorizeRoute";
import { Homepage } from "./components/Layout/Homepage";
import { FolderDetail } from "./components/Layout/FolderDetail";


export const routes = (
    <Routes >
        <Route path="/" element={<Navigate to={"/recent"} />} />
        <Route path="/recent" element={<Homepage />} />
        <Route path="/folder/:folderid" element={<FolderDetail />} />
    </Routes>
)