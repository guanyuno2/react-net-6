// @flow 
import * as React from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import * as Utils from '../../env'
import { IFolder } from '../ListFolder/_interfaces';
import './index.css'


type Props = {
    id: string
};
type Params = {

}
export const Path = (props: Props) => {
    const [lsFolder, setLsFolder] = useState<IFolder[]>([])
    const navigate = useNavigate()

    useEffect(() => {
        if (props.id?.length > 0) {
            initData()
        }
    }, [props.id])

    const initData = async () => {
        let rsp = await Utils.api_call_get(`path/${props.id}`)
        if (!rsp.error) {
            setLsFolder(rsp.data)
        }
    }

    const getStringDivide = ">"

    return (
        <div className='drive-path'>
            <div
                onClick={() => navigate("/")}
                className="quick-access-title drive-path-item">My Files</div>
            {lsFolder?.map(a => {
                let txt = `drive-path-txt`
                if (a.id == props.id)
                    txt += " current"
                return <div key={a.id} className='drive-path-item' onClick={() => navigate(`/folder/${a.id}`)}>
                    <span className="drive-path-divide">{getStringDivide}</span>
                    <span className={txt}>{a.name}</span>
                </div>
            })}
        </div>
    );
};