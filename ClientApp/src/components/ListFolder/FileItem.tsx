// @flow 
import * as React from 'react';
import { IFile, IFolder } from './_interfaces';
import { EFileType, IconFileType } from '../IconFileType';
import * as Utils from '../../env'
type Props = {
    item: IFile
};
export const FileItem = (props: Props) => {
    if (props.item == null) return null

    const renderIcon = () => {
        if (props.item.url.includes("jpg") || props.item.url.includes("JPG") ||
            props.item.url.includes("jpeg") || props.item.url.includes("jpeg") ||
            props.item.url.includes("png") || props.item.url.includes("PNG"))
            return <img src={props.item.url} />
        return <IconFileType type={Utils.getFileType(props.item.fileName)} size={50} />
    }

    const renderTitle = () => {
        let splited = props.item.url.split('/')
        return splited?.length > 0 ? splited[splited.length - 1] : "undefined"
    }

    const renderIconFile = () => {
        let file = props.item
        return <IconFileType type={Utils.getFileType(file.fileName)} size={16} />
    }

    return (
        <div key={props.item.id} className='folder-item'>
            <div className='folder-item-title file-item-title'>
                {renderIconFile()}
                <span className="folder-item-title-txt" title={props.item.url}>
                    {renderTitle()}
                </span>
            </div>
            <div className='folder-item-content file-item-content'>
                {renderIcon()}
            </div>
        </div>
    );
};

const iconFile = (size: number) => {
    return <svg xmlns="http://www.w3.org/2000/svg" height={size} width={size} viewBox="0 0 384 512"><path d="M0 64C0 28.7 28.7 0 64 0H224V128c0 17.7 14.3 32 32 32H384V448c0 35.3-28.7 64-64 64H64c-35.3 0-64-28.7-64-64V64zm384 64H256V0L384 128z" /></svg>
}
