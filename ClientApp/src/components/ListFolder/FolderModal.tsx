// @flow 
import { Button, Input, Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, ModalOverlay } from '@chakra-ui/react';
import * as React from 'react';
import { useState } from 'react';
import { IFolder } from './_interfaces';
import useEventListener from '../UseEventListener';
import { useParams } from 'react-router-dom';
type Props = {
    isOpen: boolean
    afterOnClose: () => void
    folder: IFolder
    onSubmit: (folder: IFolder) => void
};

type Params = {
    folderid: string
}

export const FolderModal = (props: Props) => {
    const [folder, setFolder] = useState<IFolder>(props.folder)
    const { folderid } = useParams<Params>()

    const handlerOnKeyDown = event => {
        if (event.code == 'Enter' && folder.name?.length > 0) {
            setFolder(props.folder)
            if (folderid?.length > 0)
                folder.parentId = folderid
            props.onSubmit(folder)
            // props.afterOnClose()
        }
    }

    useEventListener('keydown', handlerOnKeyDown)


    return props.isOpen && (
        <div>
            <Modal isOpen onClose={() => props.afterOnClose()}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>New Folder</ModalHeader>
                    <ModalBody>
                        <Input autoFocus
                            value={folder.name} onChange={(e) => {
                                let obj = { ...folder }
                                obj.name = e.target.value
                                setFolder(obj)
                            }} placeholder='Untitled folder' size='lg' />
                    </ModalBody>
                    <ModalFooter>
                        <Button colorScheme='gray' mr={3} onClick={() => props.afterOnClose()}>
                            Cancel
                        </Button>
                        <Button colorScheme='twitter' onClick={() => props.onSubmit(folder)}>Create</Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </div>
    );
};