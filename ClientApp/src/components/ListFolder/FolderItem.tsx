// @flow 
import * as React from 'react';
import { IFolder } from './_interfaces';
import { useNavigate } from 'react-router-dom';
type Props = {
    item: IFolder
};
export const FolderItem = (props: Props) => {
    const navigate = useNavigate()
    
    if (props.item == null) return null
    return (
        <div key={props.item.id} className='folder-item' onDoubleClickCapture={() => {
            navigate(`/folder/${props.item.id}`)
        }}>
            <div className='folder-item-title'>
                {iconFolder(16)}
                <span className="folder-item-title-txt" title={props.item.name}>
                    {props.item.name}
                </span>
            </div>
            <div className='folder-item-content'>
                {iconFolder(50)}
            </div>
        </div>
    );
};

const iconFolder = (size: number) => {
    return <svg width={size} height={size} viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path d="M15.8346 5H10.5846L10.3346 4.25C10.0013 3.16667 9.08464 2.5 8.0013 2.5H4.16797C2.7513 2.5 1.66797 3.58333 1.66797 5V15.8333C1.66797 17.25 2.7513 18.3333 4.16797 18.3333H15.8346C17.2513 18.3333 18.3346 17.25 18.3346 15.8333V7.5C18.3346 6.08333 17.2513 5 15.8346 5Z" fill="#81899B" />
    </svg>
}
