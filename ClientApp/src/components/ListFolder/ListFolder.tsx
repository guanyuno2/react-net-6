// @flow 
import * as React from 'react';
import { useEffect, useState } from 'react';
import { IFile, IFolder } from './_interfaces';
import * as Utils from '../../env'
import { FolderItem } from './FolderItem';
import './index.css'
import { CustomContextWrapper } from '../ContextMenu/CustomContextWrapper';
import { FolderModal } from './FolderModal';
import { Button, Divider, Menu, MenuButton, MenuItem, MenuList, useToast } from '@chakra-ui/react';
import { FileItem } from './FileItem';
import { plus } from '../Layout/LeftMenu';
import { UploadFile } from '../ContextMenu/UploadFile';


type Props = {
    id?: string
};
export const ListFolder = (props: Props) => {
    const [folders, setFolders] = useState<IFolder[]>([])
    const [files, setFiles] = useState<IFile[]>([])
    const [isOpenModalNewFolder, setIsOpenNewFolder] = useState<boolean>(false)
    const toast = useToast()

    useEffect(() => {
        initData()
    }, [props.id])

    const createFolder = async (folder: IFolder) => {
        setIsOpenNewFolder(false)
        await Utils.createFolder(folder, (isSuccess: boolean) => {
            if (isSuccess) {
                toast({
                    title: `Folder ${folder.name} created sucessfully.`,
                    // description: "We've created your account for you.",
                    status: 'success',
                    duration: 3000,
                    isClosable: true
                })
            }
            else {
                toast({
                    title: `Error`,
                    // description: "We've created your account for you.",
                    status: 'error',
                    duration: 3000,
                    isClosable: true
                })
            }

            initData()
        })
    }

    const initData = async () => {
        let pathFolder = props.id?.length > 0 ? `myfolder?id=${props.id}` : 'myfolder'
        let pathFile = props.id?.length > 0 ? `myfile?id=${props.id}` : 'myfile'
        let rsp = await Utils.api_call_get(pathFolder)
        if (rsp?.data) {
            setFolders(rsp.data)
        }

        let rspFile = await Utils.api_call_get(pathFile)
        if (rspFile?.data) {
            setFiles(rspFile.data)
        }
    }

    const renderEmpty = () => {
        return <div className='drive-empty'>
            {iconEmpty(250)}
            <div style={{}}>
                <Menu>
                    <MenuButton as={Button}
                        padding={'1.125rem 1.25rem 1.125rem 1rem'}
                        fontWeight={500}
                        minWidth={'80px'}
                        marginTop={'20px'}
                        height={'auto'}
                        borderRadius={'16px'}
                        backgroundColor={"#EDF2FC"}
                        _hover={{
                            backgroundColor: "#EDF1FA"
                        }}
                    >
                        <div className='drive-add-new'>
                            {plus}
                            New
                        </div>
                    </MenuButton>
                    <MenuList>
                        <MenuItem onClick={() => {
                            setIsOpenNewFolder(true)
                        }}>New Folder</MenuItem>
                        <MenuItem onClick={() => { }}>
                            <UploadFile uploadCallback={() => initData()} />
                        </MenuItem>
                    </MenuList>
                </Menu>
            </div>
        </div>
    }

    return (
        <>
            {folders.length == 0 && files.length == 0 &&
                renderEmpty()
            }
            <div className='list-folder-files'>
                {folders?.length > 0 && folders.map(a => {
                    return <FolderItem item={a} />
                })}

                {files?.length > 0 && files.map(a => {
                    return <FileItem item={a} />
                })}


            </div>
            <CustomContextWrapper
                onSelected={() => { }}
                uploadCallback={() => initData()}
                targetId={"main_folder_root"}
                onClick={(type) => {
                    if (type == "New_Folder")
                        setIsOpenNewFolder(true)
                }}
            />
            <FolderModal folder={{ name: "" }}
                isOpen={isOpenModalNewFolder}
                afterOnClose={() => setIsOpenNewFolder(false)}
                onSubmit={(folder) => createFolder(folder)}
            />
        </>
    );
};


const iconEmpty = (size: number) => {
    return <svg width={size} height={size} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 122.88 117.85"><path xmlns="http://www.w3.org/2000/svg" d="M60.05,68.41L18.02,46.33L6.2,60.47c14.91,7.88,29.82,15.76,44.74,23.65L60.05,68.41L60.05,68.41z M63.3,23.47L21.12,44.39 L62.24,66l41.2-21.65L79.81,31.99L63.3,23.47L63.3,23.47z M15.89,43.95L0.39,29.27c-0.57-0.65-0.51-1.65,0.15-2.23 c0.09-0.08,0.2-0.15,0.3-0.21L47.95,0.18c0.75-0.4,1.68-0.12,2.1,0.61l13.23,18.22L72.1,2.49c0.41-0.77,1.36-1.07,2.13-0.66 l47.8,25.27c0.14,0.08,0.27,0.17,0.39,0.29c0.61,0.62,0.6,1.62-0.02,2.23l-14.16,13.91l13.71,16.41c0.56,0.67,0.47,1.67-0.2,2.22 c-0.1,0.08-0.21,0.15-0.32,0.21l-12.31,6.51v24.4c0,0.66-0.4,1.22-0.98,1.46l-44.51,22.59c-0.29,0.32-0.71,0.52-1.17,0.52 c-0.62,0-1.15-0.35-1.41-0.87l-44.59-22.3c-0.55-0.28-0.87-0.83-0.87-1.41L15.59,69L3.05,62.37c-0.11-0.06-0.22-0.13-0.32-0.21 c-0.67-0.56-0.76-1.56-0.2-2.22L15.89,43.95L15.89,43.95z M60.84,21.06L48.11,3.67L4.07,28.7L18.46,42.1L60.84,21.06L60.84,21.06z M65.75,21.08l14.75,7.72l25.08,12.93l13.07-12.84L74.15,5.35L65.75,21.08L65.75,21.08z M106.47,46.33L64.44,68.41l9.11,15.7 l44.74-23.65L106.47,46.33L106.47,46.33z" /></svg>
}