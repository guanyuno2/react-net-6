export interface IBase {
    createdAt?: Date
    updatedAt?: Date
    createdBy?: string
    updatedBy?: string
    id?: string
}

export interface IFolder extends IBase {
    name?: string
    parentId?: string
}

export interface IFile extends IBase {
    url?: string
    fileName?: string
    parentId?: string
}