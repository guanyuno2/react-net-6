import * as signalR from "@microsoft/signalr";

let connection = null;

export const connectionConfig = (link: string, callback: Function) => {
    if (!connection) {
        
        connection = new signalR.HubConnectionBuilder()
            .withUrl(link)
            .configureLogging(signalR.LogLevel.Information)
            .build();
        window['HubConnection'] = connection
        initConnection();
    }
    callback(connection);
}

const initConnection = () => {
    connection.start().catch(err => { console.log('error start...', err) });

    connection.onclose(err => {
        tryConnect(err);
    })
}

const tryConnect = (err) => {
    setTimeout(() => {
        connection.start().catch(err => {
            tryConnect(err);
        });;
    }, 1000);
}