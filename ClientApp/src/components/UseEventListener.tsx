
import {useRef, useEffect}  from 'react';

const useEventListener = (
    eventName: keyof HTMLElementEventMap | keyof WindowEventMap,
    handler: any,
    element: any = window,
) => {
    const savedHandler = useRef();

    useEffect(() => {
        savedHandler.current = handler;
    }, [handler]);

    useEffect(() => {
        const isSupported = element && element.addEventListener;
        if (!isSupported) {
            return;
        }

        const eventListener = (event) => (savedHandler as any).current(event);
        element.addEventListener(eventName, eventListener);
        return () => {
            element.removeEventListener(eventName, eventListener);
        };
    }, [eventName, element]);
};

export default useEventListener;