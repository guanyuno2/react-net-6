// @flow 
import * as React from 'react';
import './index.css'
import { useRef, useState, useCallback } from 'react';
import useEventListener from '../UseEventListener';
import { Input, InputGroup, InputLeftElement, InputRightElement, Menu, MenuItem, Popover, PopoverBody, PopoverContent, PopoverHeader, PopoverTrigger, Portal, border } from '@chakra-ui/react'
import _debounce from 'lodash/debounce';
import { IFile, IFolder } from '../ListFolder/_interfaces';
import * as Utils from '../../env'
import { useNavigate } from 'react-router-dom';
import { IconFileType } from '../IconFileType';


type Props = {
    name: string
};
export const Header = (props: Props) => {
    const inputRef = useRef(null)
    const parentRef = useRef(null)
    const [lsFile, setLsFile] = useState<IFile[]>([] as IFile[])
    const [valueSearch, setValueSearch] = useState<string>("")
    const navigate = useNavigate()

    const handlerOnKeyDown = (e) => {
        // keyCode == 191: dấu / ; keyCode == 111: dấu / trên bàn phím số
        if ((e?.keyCode == 191 || e?.keyCode == 111)) {
            if (inputRef?.current)
                inputRef.current.focus()
        }
        // if (!isShowSearch) return
        if (e?.keyCode == 27) { //escape
            // onClearCache()
            // return
        }
        if (e?.keyCode == 40) { //down
            // if (indexFocusItem < (lsMatchData.length - 1)) {
            //     setIndexFocustItem(indexFocusItem + 1)
            //     scrollTo(indexFocusItem + 1, 'down')
            // }
        }
        if (e?.keyCode == 38) { //up
            // if (indexFocusItem > 0) {
            //     setIndexFocustItem(indexFocusItem - 1)
            //     scrollTo(indexFocusItem - 1, 'up')
            // }
        }
        if (e?.keyCode == 13) { //enter
            // let corresItem = lsMatchData[indexFocusItem]
            // if (corresItem)
            //     onSelectItem(corresItem)
        }
    }

    useEventListener('keydown', handlerOnKeyDown)

    const debounceSearch = useCallback(_debounce((nextValue) => callAPISearch(nextValue), 300), [])

    const callAPISearch = async (val: string) => {
        let rspFolder = await Utils.api_call_get(`file/search?query=${val}`)
        if (!rspFolder.error)
            setLsFile(rspFolder.data)
    }

    const shortContent = () => {
        let name = props.name
        let shortName = ''
        if (name && name !== '') {
            let arr = name.split(' ');
            if (arr && arr.length > 0) {
                if (arr.length <= 1) {
                    shortName = arr[0].substring(0, 1)
                } else {
                    shortName = arr[arr.length - 2].substring(0, 1) + arr[arr.length - 1].substring(0, 1)
                }
            }
        }
        if (shortName !== '')
            shortName = shortName.toUpperCase()

        return shortName
    }

    const userName = shortContent()
    const openSSO = () => {
        var a = document.createElement('a');
        var url = document['user']?.authority
        a.href = url;
        a.target = '_blank'
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }

    const renderUserProfile = () => {
        // return <div className='user-avatar'>
        //     {userName}
        // </div>
        return <Popover>
            <PopoverTrigger children={<div className='user-avatar'>
                {userName}
            </div>} />
            <PopoverContent borderColor='transparent'>
                <div className="user-avatar-container">
                    <div className="user-avatar-title">Hi, {props.name}!</div>
                    <div className="user-avatar-sso" onClick={() => {
                        openSSO()
                    }}>Manage your account</div>
                </div>
            </PopoverContent>
        </Popover>
    }

    const handleOnChange = (e) => {
        let val = e.target.value
        setValueSearch(val)
        if (val == "")
            clearAll()
        debounceSearch(val)
    }

    const handleOnGoToFile = (id: string) => {
        clearAll()
        navigate(id?.length > 0 ? `/folder/${id}` : '/')
    }

    const renderSearchItem = () => {
        if (lsFile.length == 0 && valueSearch?.length > 0)
            return <div className="search-wrapper" style={{ color: "#637C8D", padding: "8px 12px" }}>
                No recent items match your search
            </div>

        return lsFile.length > 0 && <div className="search-wrapper">
            <Menu>
                {lsFile.map(a => {
                    return <MenuItem key={a.id} className="search-item-container" onClick={() => handleOnGoToFile(a.parentId)}>
                        <div className={"search-item"}>
                            <span style={{ marginRight: '8px' }}>{<IconFileType size={12} type={Utils.getFileType(a.fileName)} />}</span>
                            <span>{a.fileName}</span>
                            <span style={{ marginLeft: 'auto' }}>{Utils.parseDate(a.updatedAt + "")}</span>
                        </div>
                    </MenuItem>
                })}
            </Menu>
        </div>
    }

    const clearAll = () => {
        setValueSearch("")
        setLsFile([])
    }

    const searchComponent = () => {
        return <div className='search-all' ref={parentRef}>
            {/* <input placeholder='Search in Drive' type='text' value={valueSearch} ref={inputRef} /> */}
            <InputGroup>
                <InputLeftElement pointerEvents='none'>
                    {searchIcon}
                </InputLeftElement>
                <Input type='text' ref={inputRef} value={valueSearch}
                    onChange={val => handleOnChange(val)}
                    placeholder='Search in Drive' />
                {valueSearch?.length > 0 &&
                    <InputRightElement
                        className={"search-clear"}
                        onClick={() => {
                            clearAll()
                        }}>
                        {cancel}
                    </InputRightElement>
                }
            </InputGroup>
            <Portal containerRef={parentRef} children={renderSearchItem()} />
        </div>
    }
    return (
        <div className='header'>
            <div>
                {searchComponent()}
            </div>
            <div className='user'>
                {bell}
                <div className='user-hi'>
                    Hi, {props.name}
                </div>
                {renderUserProfile()}
            </div>
        </div>
    );
};

const bell = <svg xmlns="http://www.w3.org/2000/svg" height="16px" width="14px" viewBox="0 0 448 512"><path d="M224 0c-17.7 0-32 14.3-32 32V51.2C119 66 64 130.6 64 208v25.4c0 45.4-15.5 89.5-43.8 124.9L5.3 377c-5.8 7.2-6.9 17.1-2.9 25.4S14.8 416 24 416H424c9.2 0 17.6-5.3 21.6-13.6s2.9-18.2-2.9-25.4l-14.9-18.6C399.5 322.9 384 278.8 384 233.4V208c0-77.4-55-142-128-156.8V32c0-17.7-14.3-32-32-32zm0 96c61.9 0 112 50.1 112 112v25.4c0 47.9 13.9 94.6 39.7 134.6H72.3C98.1 328 112 281.3 112 233.4V208c0-61.9 50.1-112 112-112zm64 352H224 160c0 17 6.7 33.3 18.7 45.3s28.3 18.7 45.3 18.7s33.3-6.7 45.3-18.7s18.7-28.3 18.7-45.3z" /></svg>

const searchIcon = <svg xmlns="http://www.w3.org/2000/svg" height="16" width="16" viewBox="0 0 512 512"><path d="M416 208c0 45.9-14.9 88.3-40 122.7L502.6 457.4c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L330.7 376c-34.4 25.2-76.8 40-122.7 40C93.1 416 0 322.9 0 208S93.1 0 208 0S416 93.1 416 208zM208 352a144 144 0 1 0 0-288 144 144 0 1 0 0 288z" /></svg>

const cancel = <svg xmlns="http://www.w3.org/2000/svg" height="16" width="12" viewBox="0 0 384 512"><path d="M342.6 150.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L192 210.7 86.6 105.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L146.7 256 41.4 361.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L192 301.3 297.4 406.6c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L237.3 256 342.6 150.6z" /></svg>