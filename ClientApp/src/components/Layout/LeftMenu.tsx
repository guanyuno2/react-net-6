// @flow 
import * as React from 'react';
import { useEffect } from "react";
import './index.css'
import { Button, Divider, Menu, MenuButton, MenuItem, MenuList, Popover, PopoverArrow, PopoverBody, PopoverCloseButton, PopoverContent, PopoverHeader, PopoverTrigger, useEventListener, useToast } from '@chakra-ui/react';
import { useState } from 'react';
import { FolderModal } from '../ListFolder/FolderModal';
import { IFolder } from '../ListFolder/_interfaces';
import * as Utils from '../../env'
import { NavLink, useLocation } from 'react-router-dom';
import { connectionConfig } from '../SignalRConfig';

type Props = {

};
export const LeftMenu = (props: Props) => {
    const [isOpenModalNewFolder, setIsOpenNewFolder] = useState<boolean>(false)
    const toast = useToast()
    const location = useLocation();

    useEffect(() => {
        registerSignalR()
    }, [])

    const registerSignalR = () => {
        if (window['HubConnection']) {
            let connection = window['HubConnection'];
            listenSignalR(connection)
        }
        else {
            connectionConfig('/iofty_hub', connection => {
                if (connection) {
                    listenSignalR(connection)
                }
            })
        }
    }

    const listenSignalR = (connection) => {
        connection.on("Ping", (data) => {
            // this.updateNewInternalMailRealTime(data)
            let aaa = data
            debugger
        });
    }

    const onClickHomepage = () => {
        window.location.href = '/'
    }

    const createFolder = async (folder: IFolder) => {
        await Utils.createFolder(folder, (isSuccess) => {
            if (isSuccess) {
                setIsOpenNewFolder(false)
                toast({
                    title: `Folder ${folder.name} created sucessfully.`,
                    // description: "We've created your account for you.",
                    status: 'success',
                    duration: 3000,
                    isClosable: true
                })
            }
            else {
                toast({
                    title: `Error`,
                    // description: "We've created your account for you.",
                    status: 'error',
                    duration: 3000,
                    isClosable: true
                })
            }
        })
    }

    return (
        <div className='nav-bar'>
            <div onClick={() => onClickHomepage()} className='header-icon'>{icon} Drive</div>
            <div >
                {/* <Menu>
                    <MenuButton as={Button}
                        padding={'1.125rem 1.25rem 1.125rem 1rem'}
                        fontWeight={500}
                        minWidth={'120px'}
                        marginTop={'20px'}
                        height={'auto'}
                        borderRadius={'16px'}
                        boxShadow={'0px 3px 1px -2px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 1px 5px 0px rgba(0,0,0,0.12)'}
                        backgroundColor={"#EDF2FC"}
                        _hover={{
                            boxShadow: '0px 2px 4px -1px rgba(0,0,0,0.2),0px 4px 5px 0px rgba(0,0,0,0.14),0px 1px 10px 0px rgba(0,0,0,0.12)',
                            backgroundColor: "#EDF1FA"
                        }}
                    >
                        <div className='drive-add-new'>
                            {plus}
                            New
                        </div>
                    </MenuButton>
                    <MenuList>
                        <MenuItem onClick={() => {
                            // setIsOpenNewFolder(true)
                        }}>New Folder</MenuItem>
                        <Divider margin={'2px'} />
                        <MenuItem onClick={() => { }}>New File</MenuItem>
                    </MenuList>
                </Menu> */}
                {/* <FolderModal folder={{ name: "" }}
                    isOpen={isOpenModalNewFolder}
                    afterOnClose={() => setIsOpenNewFolder(false)}
                    onSubmit={(folder) => createFolder(folder)}
                /> */}
            </div>

            <div className='left-menu-list-menu'>
                <NavLink
                    to={"/share"}
                    className={`left-menu-item ${location.pathname == '/share' ? 'active' : ''}`}
                >
                    {share}
                    Share with me
                </NavLink>
                <NavLink
                    to={"/recent"}
                    className={`left-menu-item ${location.pathname == '/recent' ? 'active' : ''}`}
                >
                    {recent}
                    Recent
                </NavLink>
                <NavLink
                    to={"/starred"}
                    className={`left-menu-item ${location.pathname == '/starred' ? 'active' : ''}`}
                >
                    {star}
                    Starred
                </NavLink>
                <NavLink
                    to={"/trash"}
                    className={`left-menu-item ${location.pathname == '/trash' ? 'active' : ''}`}
                >
                    {trash}
                    Trash
                </NavLink>
            </div>
        </div>

    );
};


const icon = <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" width="40px" height="40px"><linearGradient id="UC11dU6fpD1Whu1sZEZF0a" x1="14.296" x2="14.296" y1="42.833" y2="53.022" gradientUnits="userSpaceOnUse" spreadMethod="reflect"><stop offset="0" stopColor="#6dc7ff" /><stop offset="1" stopColor="#e6abff" /></linearGradient><path fill="url(#UC11dU6fpD1Whu1sZEZF0a)" d="M11.702,43.702l6.596,6.596C18.926,50.926,18.481,52,17.593,52H12c-1.105,0-2-0.895-2-2 v-5.593C10,43.519,11.074,43.074,11.702,43.702z" /><linearGradient id="UC11dU6fpD1Whu1sZEZF0b" x1="32" x2="32" y1="8" y2="55.938" gradientUnits="userSpaceOnUse" spreadMethod="reflect"><stop offset="0" stopColor="#1a6dff" /><stop offset="1" stopColor="#c822ff" /></linearGradient><path fill="url(#UC11dU6fpD1Whu1sZEZF0b)" d="M55,20h-1v-6c0-1.654-1.346-3-3-3v-1c0-1.103-0.897-2-2-2H15c-1.103,0-2,0.897-2,2v1 c-1.654,0-3,1.346-3,3v6H9c-1.654,0-3,1.346-3,3v6v1v21c0,2.757,2.243,5,5,5h42c2.757,0,5-2.243,5-5V31v-2v-6 C58,21.346,56.654,20,55,20z M55,22c0.552,0,1,0.448,1,1v2.026c-0.584-0.442-1.257-0.773-2-0.925V22H55z M15,10h34v1H15V10z M13,13 h38c0.552,0,1,0.448,1,1v10h-9.929c-1.007,0-1.94,0.5-2.497,1.337l-2.813,4.218C36.575,29.833,36.265,30,35.929,30h-7.857 c-0.336,0-0.646-0.167-0.833-0.446l-2.813-4.216C23.869,24.5,22.936,24,21.929,24H12V14C12,13.448,12.448,13,13,13z M9,22h1v2.101 c-0.743,0.152-1.416,0.482-2,0.925V23C8,22.448,8.448,22,9,22z M56,51c0,1.654-1.346,3-3,3H11c-1.654,0-3-1.346-3-3V30v-1 c0-1.654,1.346-3,3-3h10.929c0.336,0,0.646,0.167,0.833,0.446l2.813,4.216C26.131,31.5,27.064,32,28.071,32h7.857 c1.007,0,1.94-0.5,2.497-1.337l2.813-4.218C41.425,26.167,41.735,26,42.071,26H53c1.654,0,3,1.346,3,3v2V51z" /><linearGradient id="UC11dU6fpD1Whu1sZEZF0c" x1="33" x2="33" y1="8" y2="55.938" gradientUnits="userSpaceOnUse" spreadMethod="reflect"><stop offset="0" stopColor="#1a6dff" /><stop offset="1" stopColor="#c822ff" /></linearGradient><path fill="url(#UC11dU6fpD1Whu1sZEZF0c)" d="M21 17H45V19H21z" /><linearGradient id="UC11dU6fpD1Whu1sZEZF0d" x1="49" x2="49" y1="8" y2="55.938" gradientUnits="userSpaceOnUse" spreadMethod="reflect"><stop offset="0" stopColor="#1a6dff" /><stop offset="1" stopColor="#c822ff" /></linearGradient><path fill="url(#UC11dU6fpD1Whu1sZEZF0d)" d="M44 50H54V52H44z" /><linearGradient id="UC11dU6fpD1Whu1sZEZF0e" x1="49" x2="49" y1="8" y2="55.938" gradientUnits="userSpaceOnUse" spreadMethod="reflect"><stop offset="0" stopColor="#1a6dff" /><stop offset="1" stopColor="#c822ff" /></linearGradient><path fill="url(#UC11dU6fpD1Whu1sZEZF0e)" d="M44 46H54V48H44z" /><linearGradient id="UC11dU6fpD1Whu1sZEZF0f" x1="32" x2="32" y1="8" y2="55.938" gradientUnits="userSpaceOnUse" spreadMethod="reflect"><stop offset="0" stopColor="#1a6dff" /><stop offset="1" stopColor="#c822ff" /></linearGradient><path fill="url(#UC11dU6fpD1Whu1sZEZF0f)" d="M27 21H37V23H27z" /><linearGradient id="UC11dU6fpD1Whu1sZEZF0g" x1="32" x2="32" y1="8" y2="55.938" gradientUnits="userSpaceOnUse" spreadMethod="reflect"><stop offset="0" stopColor="#1a6dff" /><stop offset="1" stopColor="#c822ff" /></linearGradient><path fill="url(#UC11dU6fpD1Whu1sZEZF0g)" d="M29 25H35V27H29z" /></svg>
export const plus = <svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" viewBox="0 0 448 512"><path d="M256 80c0-17.7-14.3-32-32-32s-32 14.3-32 32V224H48c-17.7 0-32 14.3-32 32s14.3 32 32 32H192V432c0 17.7 14.3 32 32 32s32-14.3 32-32V288H400c17.7 0 32-14.3 32-32s-14.3-32-32-32H256V80z" /></svg>

export const share = <svg xmlns="http://www.w3.org/2000/svg" height="16" width="14" viewBox="0 0 448 512"><path d="M64 32C28.7 32 0 60.7 0 96V416c0 35.3 28.7 64 64 64H384c35.3 0 64-28.7 64-64V96c0-35.3-28.7-64-64-64H64zM384 160c0 35.3-28.7 64-64 64c-15.4 0-29.5-5.4-40.6-14.5L194.1 256l85.3 46.5c11-9.1 25.2-14.5 40.6-14.5c35.3 0 64 28.7 64 64s-28.7 64-64 64s-64-28.7-64-64c0-2.5 .1-4.9 .4-7.3L174.5 300c-11.7 12.3-28.2 20-46.5 20c-35.3 0-64-28.7-64-64s28.7-64 64-64c18.3 0 34.8 7.7 46.5 20l81.9-44.7c-.3-2.4-.4-4.9-.4-7.3c0-35.3 28.7-64 64-64s64 28.7 64 64z" /></svg>
export const recent = <svg xmlns="http://www.w3.org/2000/svg" height="16" width="16" viewBox="0 0 512 512"><path d="M464 256A208 208 0 1 1 48 256a208 208 0 1 1 416 0zM0 256a256 256 0 1 0 512 0A256 256 0 1 0 0 256zM232 120V256c0 8 4 15.5 10.7 20l96 64c11 7.4 25.9 4.4 33.3-6.7s4.4-25.9-6.7-33.3L280 243.2V120c0-13.3-10.7-24-24-24s-24 10.7-24 24z" /></svg>
export const star = <svg xmlns="http://www.w3.org/2000/svg" height="16" width="18" viewBox="0 0 576 512"><path d="M287.9 0c9.2 0 17.6 5.2 21.6 13.5l68.6 141.3 153.2 22.6c9 1.3 16.5 7.6 19.3 16.3s.5 18.1-5.9 24.5L433.6 328.4l26.2 155.6c1.5 9-2.2 18.1-9.7 23.5s-17.3 6-25.3 1.7l-137-73.2L151 509.1c-8.1 4.3-17.9 3.7-25.3-1.7s-11.2-14.5-9.7-23.5l26.2-155.6L31.1 218.2c-6.5-6.4-8.7-15.9-5.9-24.5s10.3-14.9 19.3-16.3l153.2-22.6L266.3 13.5C270.4 5.2 278.7 0 287.9 0zm0 79L235.4 187.2c-3.5 7.1-10.2 12.1-18.1 13.3L99 217.9 184.9 303c5.5 5.5 8.1 13.3 6.8 21L171.4 443.7l105.2-56.2c7.1-3.8 15.6-3.8 22.6 0l105.2 56.2L384.2 324.1c-1.3-7.7 1.2-15.5 6.8-21l85.9-85.1L358.6 200.5c-7.8-1.2-14.6-6.1-18.1-13.3L287.9 79z" /></svg>
export const trash = <svg xmlns="http://www.w3.org/2000/svg" height="16" width="14" viewBox="0 0 448 512"><path d="M135.2 17.7L128 32H32C14.3 32 0 46.3 0 64S14.3 96 32 96H416c17.7 0 32-14.3 32-32s-14.3-32-32-32H320l-7.2-14.3C307.4 6.8 296.3 0 284.2 0H163.8c-12.1 0-23.2 6.8-28.6 17.7zM416 128H32L53.2 467c1.6 25.3 22.6 45 47.9 45H346.9c25.3 0 46.3-19.7 47.9-45L416 128z" /></svg>

