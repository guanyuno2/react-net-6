// @flow 
import * as React from 'react';
import './index.css'
import { MyFile } from './MyFile';
import { useState } from 'react';
import { CustomContextWrapper } from 'src/components/ContextMenu/CustomContextWrapper';
import { FolderModal } from 'src/components/ListFolder/FolderModal';
import * as Utils from '../../../env'
import { IFolder } from 'src/components/ListFolder/_interfaces';
import { S3UploadSample } from 'src/components/S3';
import { IconFileType } from 'src/components/IconFileType';

type Props = {

};
export const Homepage = (props: Props) => {
    const [isOpenModalNewFolder, setIsOpenNewFolder] = useState<boolean>(false)
    const renderQuickAccess = () => {
        return <div className='quick-access'>
            <div className='quick-access-title'>Quick Access</div>
            <div>
                icon content
            </div>
        </div>
    }

    const renderMyFiles = () => {
        return <MyFile />
    }

    const createFolder = async (folder: IFolder) => {
      
    }

    return (
        <>
            <div style={{ padding: '20px', backgroundColor: '#fff', borderRadius: '16px' }} id="main_folder_root">
                {/* {renderQuickAccess()} */}
                {renderMyFiles()}
                {/* <S3UploadSample /> */}
            </div>
        </>
    );
};