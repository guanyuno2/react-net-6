// @flow 
import * as React from 'react';
import { useEffect } from 'react'
import * as Utils from '../../../env'
import { ListFolder } from 'src/components/ListFolder/ListFolder';
import { CustomContextWrapper } from 'src/components/ContextMenu/CustomContextWrapper';
import { Path } from 'src/components/Path';
type Props = {

};
export const MyFile = (props: Props) => {
    return (
        <div>
            <div className='quick-access'>
                <div >
                    <Path id={''} />
                </div>
                <div >
                    <ListFolder />
                </div>
            </div>
        </div>
    );
};