// @flow 
import * as React from 'react';
import { useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { ListFolder } from 'src/components/ListFolder/ListFolder';
import { Path } from 'src/components/Path';
import * as Utils from '../../../env'


type Props = {

};
type Params = {
    folderid: string
}
export const FolderDetail = (props: Props) => {
    const { folderid } = useParams<Params>()

    useEffect(() => {
        testPingSignalR()
    }, [])

    const testPingSignalR = async () => {
        debugger
        if (window['HubConnection']) {

            let connection = window['HubConnection'];
            // await connection.start()
            await connection.invoke("Ping", "dicmmmmm");
            // try {
            //     // await connection.invoke("Ping", "asdasda", "test pinggg");
            //     // let rsp = await Utils.api_call_get("test/ping")
            //     let rp = await fetch("/test/ping", {
            //         method: "GET",
            //         body:  null,
            //         credentials: 'include',
            //         mode: 'no-cors',
            //         headers: {
            //             'Accept': 'application/json',
            //             'Content-Type': 'application/json'
            //         }
            //     });
            //     if (rp.status == 401) {
            //         // if (env.layout) {
            //         //     env.layout.openPopupAuthorizeException();
            //         //     await checkRefreshSessionFinish();
                    
            //         // }
            //     } else if (rp.status == 403) {
            //         // if (env.layout)
            //         //     env.layout.redirectPagePermission(null)
            //     } else if (rp.status == 200 || rp.status == 201 || rp.status == 304) {
            //         debugger
            //         let asp = await rp.json();

            //     }

                


            // } catch (err) {
            //     console.error(err);
            // }
        }

    }

    return (
        <div style={{ padding: '20px', backgroundColor: '#fff', borderRadius: '16px' }} id="main_folder_root">
            <div className='quick-access'>
                <div>
                    <Path id={folderid} />
                </div>
                <div>
                    <ListFolder id={folderid} />
                </div>
            </div>
        </div>
    );
};