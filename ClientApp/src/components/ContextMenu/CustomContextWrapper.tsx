// @flow 
import * as React from 'react';
import { useState, useEffect, useRef, useLayoutEffect } from 'react';
import { CustomContextMenu } from './CustomContextMenu';
import { Divider, Menu, MenuItem, MenuList, Portal } from '@chakra-ui/react';
import './index.css'
import { UploadFile } from './UploadFile';


declare type ETypeSelect = "New_Folder" | "New_File"
type Props = {
    targetId: string
    onSelected: () => void
    onClick: (type: ETypeSelect) => void
    uploadCallback: () => void
};


export const CustomContextWrapper = (props: Props) => {
    const [isOpenMenu, setisOpenMenu] = useState<boolean>(false)
    return <>
        <CustomContextMenu
            openMenu={() => setisOpenMenu(true)}
            targetId={props.targetId}
            onClickData={() => { props.onSelected() }}
            element={
                <div>
                    <Menu isOpen>
                        <MenuList>
                            <MenuItem onClick={() => {
                                props.onClick("New_Folder")
                                setisOpenMenu(false)
                            }}>New Folder</MenuItem>
                            {/* <Divider margin={'2px'} /> */}
                            <MenuItem onClick={() => { setisOpenMenu(false) }}>
                                <UploadFile uploadCallback={props.uploadCallback} />
                            </MenuItem>
                        </MenuList>
                    </Menu>
                </div>
            }
        />
    </>
};
