// @flow 
import * as React from 'react';
import { useState, useEffect, useRef, useLayoutEffect } from 'react';
import { createPortal } from 'react-dom';
import { Portal } from '@chakra-ui/react'
import './index.css'

type Props = {
    targetId: string
    element: React.ReactElement
    exceptPrefixId?: string
    onClickData: () => void
    openMenu: () => void
};
interface IContextMenu {
    visible: boolean
    posX: number
    posY: number
}
export const CustomContextMenu = (props: Props) => {
    const [contextData, setContextData] = useState<IContextMenu>({ visible: false, posX: 0, posY: 0 });
    const contextRef = useRef(null);

    useEffect(() => {
        if (contextData?.visible) {
            document.getElementsByTagName('body')[0].style.overflow = 'hidden'
            props.onClickData()
        }
        else if (!contextData?.visible) {
            document.getElementsByTagName('body')[0].style.overflow = 'auto'
        }
    }, [contextData?.visible])


    const offClickHandler = (event) => {
        if (contextRef.current && !contextRef.current.contains(event.target)) {
            setContextData({ ...contextData, visible: false })
        }
    }

    const onKeyDown = event => {
        var code = event.code;
        if (code == 'Escape') offClickHandler(event)
    }
    const contextMenuEventHandler = (event) => {
        const targetElement = document.getElementById(props.targetId)
        if(props.openMenu)
            props.openMenu()

        if (targetElement && targetElement.contains(event.target)) {
            event.preventDefault();
            setContextData({ visible: true, posX: event.clientX, posY: event.clientY })
        } else if (contextRef.current && !contextRef.current.contains(event.target)) {
            setContextData({ ...contextData, visible: false })
        }
    }

    useEffect(() => {

        document.addEventListener('contextmenu', contextMenuEventHandler, true)
        document.addEventListener('click', offClickHandler)
        document.addEventListener('keydown', onKeyDown)
        return () => {
            document.removeEventListener('contextmenu', contextMenuEventHandler, true)
            document.removeEventListener('click', offClickHandler)
            document.removeEventListener('keydown', onKeyDown)
        }
    }, [contextData, props.targetId])

    useLayoutEffect(() => {
        if (contextData.posX + contextRef.current?.offsetWidth > window.innerWidth) {
            setContextData({ ...contextData, posX: contextData.posX - contextRef.current?.offsetWidth })
        }
        if (contextData.posY + contextRef.current?.offsetHeight > window.innerHeight) {
            setContextData({ ...contextData, posY: contextData.posY - contextRef.current?.offsetHeight })
        }
    }, [contextData])

    return (
        <Portal
            children={<div ref={contextRef} 
            onClick={(event) => {
                setContextData({ ...contextData, visible: false })
            }}
            className='context-menu'
                style={{
                    display: `${contextData.visible ? 'block' : 'none'}`,
                    left: contextData.posX,
                    top: contextData.posY,
                }}>
                {props.element}
            </div>}
        />
    );
};
