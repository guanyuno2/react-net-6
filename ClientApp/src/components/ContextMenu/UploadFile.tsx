// @flow 
import * as React from 'react';
import * as Utils from '../../env'
import { S3Client, PutObjectCommand, PutObjectRequest } from "@aws-sdk/client-s3";
import { IFile } from '../ListFolder/_interfaces';
import { useToast } from '@chakra-ui/react';
import './index.css'
import { useParams } from 'react-router-dom';


type Props = {
    uploadCallback: () => void
};
type Params = {
    folderid: string
}

export const UploadFile = (props: Props) => {
    const { folderid } = useParams<Params>()

    const toast = useToast()
    const handleFileInput = async (e) => {
        let files = e.target.files
        if (files?.length > 0) {
            let fileName = files[0].name
            const params = {
                Bucket: "tydev",
                Key: fileName,
                Body: files[0] /** object body */,
            };
            const command = new PutObjectCommand(params);
            const data = await Utils.s3client.send(command);
            if (data) {
                var entity = {
                    url: `${Utils.StaticObjStorage}/${fileName}`,
                    fileName: fileName,
                    parentId: folderid
                } as IFile
                let rsp = await Utils.api_call_post("file", entity)
                if (!rsp.error) {
                    toast({
                        title: `Upload file successfully`,
                        // description: "We've created your account for you.",
                        status: 'success',
                        duration: 3000,
                        isClosable: true
                    })
                }
                else {
                    toast({
                        title: `Error`,
                        // description: "We've created your account for you.",
                        status: 'error',
                        duration: 3000,
                        isClosable: true
                    })
                }
            }
        }

        props.uploadCallback()
    };

    return (
        <>
            <label htmlFor="file-upload" className="custom-file-upload">
                New File
            </label>
            <input id="file-upload" type="file" onChange={handleFileInput} />
        </>
    );
};