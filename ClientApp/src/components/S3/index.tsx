import { useState } from "react";
import * as React from 'react';
import { S3Client, PutObjectCommand, PutObjectRequest } from "@aws-sdk/client-s3";

const ACCOUNT_ID = '353b244ce35f10ba163ed01a235a1aaa'
const ACCESS_KEY_ID = '62eeb0baf527ab79c46348caf0faac65'
const SECRET_ACCESS_KEY = '31237fe776a2396ba68c5c28a9595564ca895cb31ae36ac8ec4693fc4ebb0191'



export const S3UploadSample = () => {
    const [selectFile, setSelectFile] = useState(null);

    const client = new S3Client({
        region: "auto",
        endpoint: `https://${ACCOUNT_ID}.r2.cloudflarestorage.com`,
        credentials: {
            accessKeyId: ACCESS_KEY_ID,
            secretAccessKey: SECRET_ACCESS_KEY,
        },

    });

    const handleFileInput = (e) => {
        setSelectFile(e.target.files);
    };

    const handleUpload = async () => {
        const params = {
            Bucket: "tydev",
            Key: selectFile[0].name,
            Body: selectFile[0] /** object body */,
        };

        const command = new PutObjectCommand(params);
        const data = await client.send(command);
        
    };

    return (
        <div>
            <input type="file" onChange={handleFileInput} multiple></input>
            <button onClick={() => handleUpload()}>Upload To S3</button>
        </div>
    );
}
