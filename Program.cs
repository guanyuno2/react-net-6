using Drive.Extensions;
using Drive.Extensions.Cookie;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using iofty.SignalR;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddSingleton<CookieEventHandler>();
builder.Services.AddProxyMiddleware(builder.Configuration);
builder.Services.AddSignalRIofty(builder.Configuration);
builder.Services.AddAuthentication(options =>
    {
        options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
        options.DefaultChallengeScheme = "oidc";
        options.DefaultSignOutScheme = "oidc";

    })
    .AddCookie()
    .AddOpenIdConnect("oidc", options =>
    {
        options.MetadataAddress = builder.Configuration["App:Authority"] + "/.well-known/openid-configuration";
        options.Authority = builder.Configuration["App:Authority"];
        options.NonceCookie.SameSite = SameSiteMode.None;
        options.CorrelationCookie.SameSite = SameSiteMode.None;
        options.RequireHttpsMetadata = false;
        options.UseTokenLifetime = true;
        options.SignInScheme = "Cookies";
        options.ClientId = builder.Configuration["App:ClientId"];
        options.ClientSecret = builder.Configuration["App:ClientSecret"];
        options.ResponseType = "code";

        options.GetClaimsFromUserInfoEndpoint = true;
        options.MapInboundClaims = false;
        options.SaveTokens = true;

        options.Scope.Clear();
        options.Scope.Add("openid");
        options.Scope.Add("profile");
        options.Scope.Add("api1");

        options.TokenValidationParameters = new()
        {
            NameClaimType = "name",
            RoleClaimType = "role"
        };
});

var app = builder.Build();

app.UseForwardedHeaders(new ForwardedHeadersOptions
{
    ForwardedHeaders = ForwardedHeaders.XForwardedProto
});
app.UseDefaultFiles();
app.UseStaticFiles();
app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();
app.UseProxyMiddleware();
app.MapHub<ChatHub>("/iofty_hub");

app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

app.MapFallbackToFile("index.html"); ;
var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
Console.WriteLine("environmentName {0} - ", environmentName);
app.Run();
